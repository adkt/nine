# REQUIREMENTS  

node.js v8.9.4  


### RUNNING THE MAIN SERVER  

1. Navigate to the root folder (nine)  

2. run npm start  

	$ sudo npm start  
  

### TESTING 

- Unit Testing through mocha, just navigate to the root of this application and run "sudo mocha" after installing the node package of this application.  


- End to end testing can be done by setting up both the Front end and Back end servers like so:  

1. Start the front end server  

	$ sudo npm run-script front  

  
2. In a broswer navigate to http://serverAddress:2999  

3. Click on the "Send JSON content" button  

4. results will be posted in the console of your browser. The backend server will also output the result.  
