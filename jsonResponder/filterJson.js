exports.getDrm = function(data)
{
    // variable to return payloads that have drm
    var newData = {};
    newData.payload = new Array();
    var count = 0;
	
    // check to see that a payload exists
    if (checkPayload(data) == false)
    {
	return {};
    }

    // finding which payloads have drm and add them to newpayload
    for(var i=0; i<data.payload.length; i++)
    {
        // allowing for the drm value to be a string as well as a boolean this is to allow http text only parsing as well as Json parsing in body parser
		if (data.payload[i].drm == true || data.payload[0].drm == "true")
    	{
        	newData.payload[count] = data.payload[i];
	    	count++;
    	}
    }
    return newData;
}

exports.getAtLeastOneEpisode = function(data)
// check to see that a payload exists
{
    // variable to return payloads that have at least one drm
    var newData = {};
    newData.payload = new Array();
    var count = 0;
    
    // check to see that a payload exists
    if (checkPayload(data) == false)
    {
        return {};
    }

    // find which payloads have more than one episode and add them to newpayload
    for(var i=0; i<data.payload.length; i++)
    {
        if (data.payload[i].episodeCount > 0)
        {
            newData.payload[count] = data.payload[i];
            count++;
        }
    }
    return newData;
}

exports.packagePayloadForExport = function (data)
{
    var newPayload = {};
    newPayload.response = new Array();
    
    // check to see that a payload exists
    if (checkPayload(data) == false)
    {
        return {};
    }

    // loop through each payload item and filter out the fields required
    for(var i=0; i<data.payload.length; i++)
    {
        // initialise payload element
        newPayload.response[i] = {};

        // get image
        newPayload.response[i].image = data.payload[i].image.showImage;

        // get slug
        newPayload.response[i].slug = data.payload[i].slug;

        // get title
        newPayload.response[i].title = data.payload[i].title;
    }

    //return Array
    return newPayload;
}

var checkPayload = function(data) 
{   
    // if no payload recieved exit function
    if (data.payload == undefined || data.payload == null)
    {
        // returning an empty Object because no matches were found
        return false;
    }
    return true;
}
