var express = require('express');
var helmet = require('helmet');
var morgan = require('morgan');
var bodyParser = require('body-parser');

// external functions and Utilities
var filterJson = require('./filterJson.js');
var tools = require('./tools.js');
var logger = require('../log/logger.js');

var app = express();

// initialise http logging
// errors to std error log
app.use(morgan('dev', {
	skip: function (req, res) {
		return res.statusCode < 400
	}, 
	stream: process.stderr
}));
// sucesses to std output
app.use(morgan('dev', {
    skip: function (req, res) {
        return res.statusCode >= 400
	}, 
	stream: process.stdout
}));

// protect against malicious probes
app.use(helmet());
app.use(helmet.noCache());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// allow cors access from any external domain
app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// bodyParser error handling
app.use(function (error, request, result, next) {
	// invalid json error handle
	if (error.message == "invalid json"){	
		// send status and error object in response
  		result.status(401).json({ error: 'Could not decode request' });
		logger.error('401 request is not JSON');
	}
	else{
		next();
	}
});


// starts the server listening
app.listen(80,function(){
	logger.info('Waiting for JSON data... listening on port 80');
});

// waits for post to getJson
app.post('/',function(request, result){
	var data = filterPayload(request.body);

	// check if data is an empty object or malformed - if not then proceed to filter
	if (!data){
		// empty Json sent so send status and error object in response
        result.status(400).json({ error: 'Could not decode request' });
		logger.error('400 cannot decode JSON');
	}
	else{
		// send back response to client
        result.status(200).json(data);
	}
});

var filterPayload = function(data){
	if(!tools.isEmptyObj(data)){
        // run filters
        data = filterJson.getDrm(data);
        data = filterJson.getAtLeastOneEpisode(data);

        // package data as array with desired keys
        data = filterJson.packagePayloadForExport(data);
		
		return data;
    }
    else{
		// json object is empty
		return false;
    }
}

// export the whole app
module.exports = app;

// export functions
module.exports.filterPayload = filterPayload;
