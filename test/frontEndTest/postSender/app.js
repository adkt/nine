
var express        =         require("express");
var bodyParser     =         require("body-parser");
var app            =         express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// route for test site
app.get('/',function(req,res){
  res.sendFile("/var/nine/test/frontEndTest/postSender/index.html");
});

// start server
app.listen(2999,function(){
  console.log("Started on PORT 2999");
})
