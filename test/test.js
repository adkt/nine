var chai = require('chai');

var assert = chai.assert;
var should = chai.should();

var app = require('../jsonResponder/app.js');
var request = require('supertest')(app);

// test files to load
var defaultJson = require('./jsonTestFiles/defaultPayload.json');
var completedJson = require('./jsonTestFiles/completedPayload.json');

// start jsonResponder server here if it has not already started
if(!module.parent)
{ 
    app.listen(80); 
}

// tests
describe('JsonResponder Tests', function()
{
	describe('Server Tests', function(){
		it("Server should accept POST requests from any origin", function(done){
			request.post('').expect('Access-Control-Allow-Origin', '*', done)
		});
		
		it("POST nothing so should return error code 400", function(done) {
			request.post('/').expect(400, done);
		});
		
		it("Passing defaultPayload.json should result in a sucess(200) code ", function(done){
			request.post('/').send(defaultJson).expect(200, done)
		});

		it("Passing defaultPayload.json should return competedPayload.json", function(done){
			request.post('/').send(defaultJson).end(function(err, result) {
        		assert.deepEqual(result.body, completedJson);
        		done();
			});
		});
		
		it("Passing malformed JSON should return a 400 error", function (done){
			request.post('/').type('json').send('"test:"testing"').expect(400, done)
		});
		
	});

	describe('Filter Testing', function()
	{
    	it('Empty data should return false', function(){
			assert.isFalse(app.filterPayload(null), "Empty JSON returns false");
		});
		
		it('An Object that returns no filter results returns an empty Object', function(){
			assert.isObject(app.filterPayload({"myObject":"no data"}), "No filter results in empty Object");
		});

		it('defaultPayload.json should return as completedPayload.json', function(){
			assert.deepEqual(app.filterPayload(defaultJson), completedJson);
		});

  	});
});
